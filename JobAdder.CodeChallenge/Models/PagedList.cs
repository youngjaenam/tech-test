﻿using System.Collections.Generic;

namespace JobAdder.CodeChallenge.Models
{
  public class CandidatePagedList : IPagedList<Candidate>
  {
    public int PageSize { get; set; }
    public int PageIndex { get; set; }
    public ICollection<Candidate> Items { get; set; } = new List<Candidate>();
    public int TotalItems { get; set; }
  }

  public class JobPagedList : IPagedList<Job>
  {
    public int PageSize { get; set; }
    public int PageIndex { get; set; }
    public ICollection<Job> Items { get; set; } = new List<Job>();
    public int TotalItems { get; set; }
  }
}
