﻿using JobAdder.CodeChallenge.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;

namespace JobAdder.CodeChallenge.Services
{
  public class CandidateService : ICandidateService
  {
    public CandidateService(HttpClient httpClient)
    {
      _httpClient = httpClient ?? new HttpClient();
    }

    private readonly HttpClient _httpClient;

    public async Task<ICollection<Candidate>> FindAllAsync()
    {
      var response = await _httpClient.GetAsync("http://private-76432-jobadder1.apiary-mock.com/candidates");
      if (!response.IsSuccessStatusCode)
      {
        // Implement logging
        return new List<Candidate>();
      }
      
      try
      {
        var json = response.Content.ReadAsStringAsync().Result;

        var jsonSerializerOptions = new JsonSerializerOptions
        {
          PropertyNameCaseInsensitive = true
        };

        return JsonSerializer.Deserialize<ICollection<Candidate>>(json, jsonSerializerOptions);
      }
      catch (Exception ex)
      {
        //Implement logging
        return new List<Candidate>();
      }
    }

    public async Task<IPagedList<Candidate>> FindAsync(int pageIndex = 0, int pageSize = 10)
    {
      pageIndex = pageIndex < 0 ? 0 : pageIndex;
      pageSize = pageSize <= 0 ? 10 : pageSize;

      var pageResult = new CandidatePagedList
      {
        PageIndex = pageIndex,
        PageSize = pageSize,
      };

      try
      {
        var candidates = await FindAllAsync();
        if (candidates == null || candidates.Count < 1)
          return pageResult;

        var totalCount = candidates.Count;
        var skip = pageIndex * pageSize;
        if (skip > totalCount)
          throw new ArgumentOutOfRangeException($"Index of {pageIndex} exceeds the amount of items retrievable.");

        pageResult.Items = candidates.Skip(skip).Take(pageSize).ToList();
        pageResult.TotalItems = totalCount;

        return pageResult;
      }
      catch (Exception ex)
      {
        if (ex is JsonException)
          throw;

        // TODO implement logging
        return pageResult;
      }
    }

    public async Task<Candidate> GetByIDAsync(int candidateID)
    {
      var response = await _httpClient.GetAsync("http://private-76432-jobadder1.apiary-mock.com/candidates");
      if (!response.IsSuccessStatusCode)
      {
        // Implement logging
        return null;
      }

      try
      {
        var json = response.Content.ReadAsStringAsync().Result;

        var jsonSerializerOptions = new JsonSerializerOptions
        {
          PropertyNameCaseInsensitive = true
        };

        var candidates = JsonSerializer.Deserialize<ICollection<Candidate>>(json, jsonSerializerOptions);

        return candidates.Single(x => x.CandidateId == candidateID);
      }
      catch (Exception ex)
      {
        // Implement logging
        return null;
      }
    }
  }
}
