﻿using System;
using JobAdder.CodeChallenge.Models;
using System.Collections.Generic;
using System.Linq;

namespace JobAdder.CodeChallenge.Services
{
  public class SkillMatchService : ISkillMatchService
  {
    public ICollection<Candidate> GetBestCandidatesForJob(Job job, IEnumerable<Candidate> candidates, int take = 5)
    {
      // Implement a simple matching algo
      var jobSkillTags = job.Skills;

      var potentialCandidates = new List<Candidate>();
      foreach (var candidate in candidates)
      {
        var intersect = candidate.SkillTags.Intersect(jobSkillTags);
        if (intersect.Any())
          potentialCandidates.Add(candidate);
      }

      return potentialCandidates;
    }

    public ICollection<Job> GetBestJobsForCandidate(Candidate candidate, IEnumerable<Job> jobs, int take = 5)
    {
      // Implement a simple matching algo
      var candidateSkillTags = candidate.SkillTags;

      var potentialJobs = new List<Job>();
      foreach (var job in jobs)
      {
        var intersect = job.Skills.Intersect(candidateSkillTags);
        if (intersect.Any())
          potentialJobs.Add(job);
      }

      return potentialJobs;
    }
  }
}
