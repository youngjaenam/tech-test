﻿using JobAdder.CodeChallenge.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;

namespace JobAdder.CodeChallenge.Services
{
  public class JobService : IJobService
  {
    public JobService(HttpClient httpClient)
    {
      _httpClient = httpClient ?? new HttpClient();
    }

    private readonly HttpClient _httpClient;

    public async Task<ICollection<Job>> FindAllAsync()
    {
      var response = await _httpClient.GetAsync("http://private-76432-jobadder1.apiary-mock.com/jobs");
      if (!response.IsSuccessStatusCode)
      {
        // Implement Logging
        return new List<Job>();
      }

      try
      {
        var json = response.Content.ReadAsStringAsync().Result;

        var jsonSerializerOptions = new JsonSerializerOptions
        {
          PropertyNameCaseInsensitive = true
        };

        return JsonSerializer.Deserialize<ICollection<Job>>(json, jsonSerializerOptions);
      }
      catch (Exception ex)
      {
        //Implement logging
        return new List<Job>();
      }
    }

    public async Task<IPagedList<Job>> FindAsync(int pageIndex = 0, int pageSize = 10)
    {
      pageIndex = pageIndex < 0 ? 0 : pageIndex;
      pageSize = pageSize <= 0 ? 10 : pageSize;

      var pageResult = new JobPagedList
      {
        PageIndex = pageIndex,
        PageSize = pageSize,
      };

      try
      {
        var jobs = await FindAllAsync();
        
        if (jobs == null || jobs.Count < 1)
          return pageResult;

        var totalCount = jobs.Count;
        var skip = pageIndex * pageSize;
        if (skip > totalCount)
          throw new ArgumentOutOfRangeException($"Index of {pageIndex} exceeds the amount of items retrievable.");

        pageResult.Items = jobs.Skip(skip).Take(pageSize).ToList();
        pageResult.TotalItems = totalCount;
        return pageResult;
      } 
      catch (Exception ex)
      {
        if (ex is JsonException)
          throw; 

        //Implment logging
        return pageResult;
      }
    }

    public async Task<Job> GetByIDAsync(int jobId)
    {
      var response = await _httpClient.GetAsync("http://private-76432-jobadder1.apiary-mock.com/jobs");
      if (!response.IsSuccessStatusCode)
      {
        // Implment logging
        return null;
      }

      try
      {
        var json = response.Content.ReadAsStringAsync().Result;

        var jsonSerializerOptions = new JsonSerializerOptions
        {
          PropertyNameCaseInsensitive = true
        };

        var jobs = JsonSerializer.Deserialize<ICollection<Job>>(json, jsonSerializerOptions);

        return jobs.Single(x => x.JobId == jobId);
      }
      catch (Exception ex)
      {
        // Implement logging 
        return null;
      }
    }
  }
}
